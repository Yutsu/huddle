const ratio = 0.6;
const option = {
    root: null,
    rootMargin: '0px',
    threshold: ratio
}

//second
const handleIntersect = ( (entries, observer) => {
    entries.map(entry => {
        if(entry.intersectionRatio > ratio) {
            entry.target.classList.add('reveal-visible'); //entry.target => div class
            observer.unobserve(entry.target); //permet d'arrêter msg quand il est passé 1x
        }
    })
});

//observer.observe(document.querySelector('.reveal')); => one target
const observer = new IntersectionObserver(handleIntersect, option);

//first x4
Array.from(document.querySelectorAll('.reveal')).forEach(element => {
    observer.observe(element);
});


const form = document.getElementById('form');
const small = document.getElementById('small');

const formSubmit = (e) => {
    e.preventDefault();
    let emailValue = form['email'].value;
    if(!emailValue) {
        small.innerHTML ="Email empty";
    }
    else if(!validateEmail(emailValue)){
        small.innerHTML ="Email invalid";
    }
    else {
        small.innerHTML ="";
    }
}
const validateEmail = (email) => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
form.addEventListener('submit', formSubmit);